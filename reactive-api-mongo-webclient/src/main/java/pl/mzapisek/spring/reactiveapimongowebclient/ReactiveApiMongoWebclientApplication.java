package pl.mzapisek.spring.reactiveapimongowebclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveApiMongoWebclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveApiMongoWebclientApplication.class, args);
    }

}
