package pl.mzapisek.spring.reactiveapimongowebclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Service
public class CarClient {

    private final WebClient webClient;

    @Autowired
    public CarClient(@Qualifier("carWebClient") WebClient webClient) {
        this.webClient = webClient;
    }

    Flux<Car> getAllCars() {
        return this.webClient
                .get()
                .uri("/cars")
                .retrieve()
                .bodyToFlux(Car.class);
    }

}
