package pl.mzapisek.spring.reactiveapimongowebclient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    private String id;
    private String make;
    private String model;
    private String productionYear;

}
