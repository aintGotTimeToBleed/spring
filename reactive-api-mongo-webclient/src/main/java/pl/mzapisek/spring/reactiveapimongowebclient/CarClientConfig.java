package pl.mzapisek.spring.reactiveapimongowebclient;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class CarClientConfig {

    @Value("${car.service.uri}")
    private String carServiceUri;

    @Bean
    @Qualifier("carWebClient")
    public WebClient webClient() {
        return WebClient.builder().baseUrl(carServiceUri).build();
    }

}
