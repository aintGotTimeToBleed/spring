package pl.mzapisek.spring.reactiveapimongowebclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static com.github.tomakehurst.wiremock.client.WireMock.get;

@SpringBootTest
@AutoConfigureJson
@AutoConfigureWireMock(port = 9991)
class CarClientTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CarClient carclient;

    @Test
    public void callingCarEndpoint_shouldReturnCarFlux() throws JsonProcessingException {

        List<Car> cars = Arrays.asList(
                new Car(null, "VW", "Passat", null),
                new Car(null, "Opel", "Corsa", null));
        String json = this.objectMapper.writeValueAsString(cars);

        WireMock.stubFor(get(WireMock.urlEqualTo("/cars")).willReturn(WireMock.aResponse()
                .withBody(json)
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .withStatus(HttpStatus.OK.value())
        ));

        StepVerifier
                .create(this.carclient.getAllCars())
                .expectNextMatches(matchCarPredicate("VW", "Passat"))
                .expectNextMatches(matchCarPredicate("Opel", "Corsa"))
                .verifyComplete();
    }

    Predicate<Car> matchCarPredicate(String make, String model) {
        return car -> car.getMake().equals(make) && car.getModel().equals(model);
    }
}
