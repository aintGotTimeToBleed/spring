package pl.mzapisek.spring.reactiveapimongo.car;

import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ReactiveCarRepository extends ReactiveCrudRepository<Car, String> {

    Flux<Car> findByMake(String make);

    @Tailable
    Flux<Car> findAllBy();
}
