package pl.mzapisek.spring.reactiveapimongo.car;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class CarRestController {

    private final ReactiveCarRepository reactiveCarRepository;
    private final RandomCarGeneratorService randomCarGeneratorService;

    @Autowired
    public CarRestController(ReactiveCarRepository reactiveCarRepository, RandomCarGeneratorService randomCarGeneratorService) {
        this.reactiveCarRepository = reactiveCarRepository;
        this.randomCarGeneratorService = randomCarGeneratorService;
    }

    @GetMapping("/cars")
    Publisher<Car> carPublisher() {
        return this.reactiveCarRepository.findAll();
    }

    @GetMapping(value = "cars/random/sse", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    Publisher<Car> carSSE() {
        return this.randomCarGeneratorService.generateRandomCar();
    }

    // TODO
    // Collection should be capped !!! Right now it isn't
    // https://codecouple.pl/2018/08/03/otwieramy-kursor-w-mongodb-tailable-cursor/
    // https://www.baeldung.com/spring-data-mongodb-tailable-cursors
    // https://docs.spring.io/spring-data/mongodb/docs/current/api/org/springframework/data/mongodb/repository/Tailable.html
    @GetMapping("/cars/sse")
    Publisher<Car> carPublisherSSE() {
        return this.reactiveCarRepository.findAllBy().doOnNext(System.out::println);
    }
}
