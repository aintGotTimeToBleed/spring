package pl.mzapisek.spring.reactiveapimongo.car;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Service
public class RandomCarGeneratorService {

    private List<String> makes = Arrays.asList("BMW", "VW", "Opel");
    private List<Integer> years = Arrays.asList(1999, 2001, 2019);

    Flux<Car> generateRandomCar() {
        return Flux.fromStream(Stream.generate(() ->
                new Car(null, makes.get(new Random().nextInt(2)), null, years.get(new Random().nextInt(2)))))
                .delayElements(Duration.ofSeconds(1));
    }
}
