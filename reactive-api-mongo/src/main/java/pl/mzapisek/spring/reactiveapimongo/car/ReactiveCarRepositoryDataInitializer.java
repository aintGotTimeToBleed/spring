package pl.mzapisek.spring.reactiveapimongo.car;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
@Slf4j
public class ReactiveCarRepositoryDataInitializer {

    private ReactiveCarRepository reactiveCarRepository;

    @Autowired
    public ReactiveCarRepositoryDataInitializer(ReactiveCarRepository reactiveCarRepository) {
        this.reactiveCarRepository = reactiveCarRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initialize() {
        log.info("data initialization for car repository started");
        Flux<Car> carFlux = Flux.just(
                new Car(null, "Audi", "A3", 2019),
                new Car(null, "Porsche", "Cayenne", 2005),
                new Car(null, "Fiat", "Uno", 1999));
        Flux<Car> savedCarFlux = carFlux.flatMap(reactiveCarRepository::save);

        this.reactiveCarRepository
                .deleteAll()
                .thenMany(savedCarFlux)
                .thenMany(this.reactiveCarRepository.findAll())
                .subscribe(car -> log.info(car.toString()));
    }
}
