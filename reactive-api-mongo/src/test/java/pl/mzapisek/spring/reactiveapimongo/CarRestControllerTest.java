package pl.mzapisek.spring.reactiveapimongo;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.mzapisek.spring.reactiveapimongo.car.Car;
import pl.mzapisek.spring.reactiveapimongo.car.ReactiveCarRepository;
import reactor.core.publisher.Flux;

/**
 * https://www.baeldung.com/spring-tests
 * https://github.com/spring-projects/spring-boot/tree/master/spring-boot-project/spring-boot-test-autoconfigure/src/main/java/org/springframework/boot/test/autoconfigure
 */
@WebFluxTest
public class CarRestControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ReactiveCarRepository reactiveCarRepository;

    @Test
    public void carsEndpoint_shouldReturnValidData() {

        Mockito.when(this.reactiveCarRepository.findAll())
            .thenReturn(Flux.just(
                    new Car(null, "BMW", null, null),
                    new Car(null, "Polonez", null, null)));

        this.webTestClient
                .get()
                .uri("/api/v1/cars")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("@.[0].make").isEqualTo("BMW")
                .jsonPath("@.[1].make").isEqualTo("Polonez");
    }
}
