package pl.mzapisek.spring.reactiveapimongo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import pl.mzapisek.spring.reactiveapimongo.car.Car;
import pl.mzapisek.spring.reactiveapimongo.car.ReactiveCarRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@DataMongoTest
public class ReactiveCarRepositoryTest {

    @Autowired
    private ReactiveCarRepository reactiveCarRepository;

    @Test
    public void persist() {
        Mono<Car> polonez = this.reactiveCarRepository.save(new Car(null, "Polonez", null, 1990));
        StepVerifier
                .create(polonez)
                .expectNextMatches(car ->
                        car.getMake().equals("Polonez") &&
                                car.getModel() == null &&
                                car.getProductionYear() == 1990)
                .verifyComplete();
    }

    @Test
    public void findByMake_shouldReturnValidData() {
        Flux<Car> carFlux = this.reactiveCarRepository.deleteAll()
                .thenMany(Flux.just("Audi", "BMW", "Polonez"))
                .map(make -> new Car(null, make, null, null))
                .flatMap(car -> this.reactiveCarRepository.save(car))
                .thenMany(this.reactiveCarRepository.findByMake("Polonez"));

        StepVerifier
                .create(carFlux)
                .expectNextCount(1)
                .verifyComplete();
    }

}
