package pl.mzapisek.spring.reactiveapimongo;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.mzapisek.spring.reactiveapimongo.car.Car;

public class CarTest {

    @Test
    public void create() {
        Car car = new Car(null, "Audi", "A5", 2001);
        Assertions.assertThat(car.getMake()).isEqualTo("Audi");
        Assertions.assertThat(car.getModel()).isEqualTo("A5");
        Assertions.assertThat(car.getProductionYear()).isEqualTo(2001);
    }

}
