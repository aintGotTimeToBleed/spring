package pl.mzapisek.spring.reactiveapimongo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.mzapisek.spring.reactiveapimongo.car.Car;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CarRestControllerWithRunningServerTest {

    private static final String BASE_URL = "http://localhost:";

    @LocalServerPort
    private Integer localServerPort;

    private WebTestClient webTestClient;

    @BeforeEach
    public void before() {
        webTestClient = WebTestClient.bindToServer()
                .baseUrl(BASE_URL + localServerPort)
                .build();
    }

    @Test
    public void findAllCars() {
        webTestClient
                .get()
                .uri( "/api/v1/cars")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Car.class)
                .hasSize(3);
    }
}
