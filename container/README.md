## Spring container

* what is a container
* what is a bean
* Scopes
* ApplicationContext
* annotations
    @Configuration
    @Bean
    @Import
    @DependsOn
    
    
https://pivotalcontent.s3.amazonaws.com/training/training-briefs/Pivotal_EDU_CoreSpring_TrainingBrief.pdf
https://www.baeldung.com/spring-factorybean