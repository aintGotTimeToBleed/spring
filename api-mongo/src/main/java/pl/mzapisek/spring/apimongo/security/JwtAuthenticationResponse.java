package pl.mzapisek.spring.apimongo.security;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JwtAuthenticationResponse {

    private String jwtToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

}
