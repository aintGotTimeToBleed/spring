package pl.mzapisek.spring.apimongo.investigation;

import org.springframework.stereotype.Service;
import pl.mzapisek.spring.apimongo.investigation.request.InvestigationRequest;
import pl.mzapisek.spring.apimongo.security.UserPrincipal;

@Service
public class InvestigationService {

    public Investigation createInvestigation(InvestigationRequest request, UserPrincipal user) {
        return new Investigation(request.getCaseId(), request.getDescription(), user.getId());
    }

}
