package pl.mzapisek.spring.apimongo.investigation.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvestigationRequest {

    @NotBlank
    @Size(max = 24)
    private String caseId;

    @Size(max = 512)
    private String description;

}
