package pl.mzapisek.spring.apimongo;

import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(SignatureException.class)
    public final ResponseEntity handleJWTSignatureException(Exception ex, WebRequest webRequest) {
        log.error("Invalid JWT signature. Exception message: {}. WebRequest {}",
                ex.getMessage(),
                webRequest.getHeader(HttpHeaders.AUTHORIZATION));

        return new ResponseEntity<>("Invalid JWT signature", HttpStatus.UNAUTHORIZED);
    }

    // MalformedJwtException - Invalid JWT token
    // ExpiredJwtException - Expired JWT token
    // UnsupportedJwtException - Unsupported JWT token
    // IllegalArgumentException - JWT claims string is empty

}
