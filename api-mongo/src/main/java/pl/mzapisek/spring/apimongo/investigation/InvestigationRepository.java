package pl.mzapisek.spring.apimongo.investigation;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface InvestigationRepository extends MongoRepository<Investigation, String> {

    Optional<Investigation> findByCaseId(String caseId);

}
