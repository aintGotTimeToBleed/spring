package pl.mzapisek.spring.apimongo.user.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
@ToString
public class LoginRequest {

    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min = 8, max = 64)
    private String password;

}
