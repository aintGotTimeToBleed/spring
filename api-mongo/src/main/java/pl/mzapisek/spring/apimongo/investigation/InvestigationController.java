package pl.mzapisek.spring.apimongo.investigation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.mzapisek.spring.apimongo.ApiResponse;
import pl.mzapisek.spring.apimongo.investigation.request.InvestigationRequest;
import pl.mzapisek.spring.apimongo.security.UserPrincipal;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class InvestigationController {

    private InvestigationService investigationService;
    private InvestigationRepository investigationRepository;

    @Autowired
    public InvestigationController(InvestigationService investigationService, InvestigationRepository investigationRepository) {
        this.investigationService = investigationService;
        this.investigationRepository = investigationRepository;
    }

    @PostMapping
    @PreAuthorize("hasRole('USER_ADMIN')")
    public ResponseEntity<?> createInvestigation(
            @Valid @RequestBody InvestigationRequest request,
            @AuthenticationPrincipal UserPrincipal user) {
        Investigation investigation = this.investigationService.createInvestigation(request, user);
        investigationRepository.save(investigation);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{investigationId}")
                .buildAndExpand(investigation.getId()).toUri();

        return ResponseEntity.created(location).body(
                new ApiResponse(true, "Investigation created", HttpStatus.CREATED.value()));
    }

    @GetMapping("/investigations/{caseId}")
    public Optional<Investigation> getInvestigations(@PathVariable String caseId, @AuthenticationPrincipal UserPrincipal user) {
        return this.investigationRepository.findByCaseId(caseId);
    }

    @GetMapping("/test")
    public String test(@AuthenticationPrincipal UserPrincipal user) {
        return "ok";
    }

}
