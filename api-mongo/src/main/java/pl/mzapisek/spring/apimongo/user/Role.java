package pl.mzapisek.spring.apimongo.user;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
