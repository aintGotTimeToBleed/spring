package pl.mzapisek.spring.apimongo.investigation;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Document("investigation")
@NoArgsConstructor
@Data
public class Investigation {

    @Id
    private String id;

    @NotBlank
    @Size(max = 24)
    private String caseId;

    @Size(max = 512)
    private String description;

    @NotBlank
    private String userId;

    @PersistenceConstructor
    public Investigation(@NotBlank @Size(max = 24) String caseId, @Size(max = 512) String description, @NotBlank String userId) {
        this.caseId = caseId;
        this.description = description;
        this.userId = userId;
    }
}
