package pl.mzapisek.spring.apimongo.user;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByEmail(String email);

    // todo remove?
    Optional<User> findByName(String name);

    Boolean existsByEmail(String email);
}
